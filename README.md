# SysFo-RS

__Author:__ Grant Wade \<grant.wade@meridiansky.co\>

__Version:__ v0.1.0

## Description
SysFo-RS is a simple crate for getting information about the platform that Rust
is currently running on. The goal is to have an Enum that represents the four
main operating systems (Windows, MacOS, Linux, and BSD), along with a way to
see what shell is running and any environment variables that are needed by the
caller. This crate is intended to be a simple way to get basic system info,
so it won't be identifying different linux/bsd distrobutions as that is beyond
the scope of this library. As an aside no external crates are used to ensure
compatibility. This is implemented using configuration flags that your compiler
will set. This is the most painless way to get the platform and it works with
cross compilation too!


## Installation
Just like any Rust crate installing is as easy as adding a line to `Cargo.toml`

__crate.io version:__
```toml
# Not yet on crates.io
sysfo-rs = "0.0.1"
```

__git verion:__
```toml
sysfo = { git = "https://gitlab.com/grant-wade/sysfo-rs.git" }
```

## Example

```rust
extern crate sysfo

use sysfo::{platform, shell, environ};

fn main() {
    let platform = platform::Platform::get();

    let shell = shell::Shell::get();
}
```
