
pub mod platform {
    #[derive(std::fmt::Debug)]
    pub enum Platforms {
        Windows,
        MacOS,
        Linux
    }

    #[derive(Debug)]
    pub struct Platform {
        pub platform: Platforms,
        pub arch: String,
        pub name: String
    }

    impl std::fmt::Display for Platform {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "Platform: {:?}, Arch: {}, Name: {})", self.platform, self.arch, self.name)
        }
    }

    impl Platform {
        pub fn new(platform: Platforms, arch: String, name: String) -> Self {
            Platform {
                platform,
                arch,
                name
            }
        }

        fn get_arch() -> String {
            let arch = if cfg!(target_arch = "x86") {
                String::from("x86")
            } else if cfg!(target_arch = "x86_64") {
                String::from("x86_64")
            } else if cfg!(target_arch = "mips") {
                String::from("mips")
            } else if cfg!(target_arch = "powerpc") {
                String::from("powerpc")
            } else if cfg!(target_arch = "powerpc64") {
                String::from("powerpc64")
            } else if cfg!(target_arch = "arm") {
                String::from("arm")
            } else if cfg!(target_arch = "aarch64") {
                String::from("aarch64")
            } else {
                String::from("unknown")
            };
            arch
        }

        #[cfg(target_os = "macos")]
        pub fn get() -> Self {
            Platform::new(
                Platforms::MacOS,
                Platform::get_arch(),
                String::from("name")
            )
        }

        #[cfg(target_os = "windows")]
        pub fn get() -> Self {
            Platform::new(
                Platforms::Windows,
                Platform::get_arch(),
                String::from("name")
            )
        }

        #[cfg(target_os = "linux")]
        pub fn get() -> Self {
            let hostname = match std::fs::read_to_string("/etc/hostname") {
                Ok(n) => n,
                Err(_) => String::from("unkown")
            };

            Platform::new(
                Platforms::Linux,
                Platform::get_arch(),
                hostname
            )
        }
    }
}


pub mod shell {
    #[derive(Debug)]
    pub struct Shell {
        pub name: String,
        pub location: String,
        pub term: String
    }

    impl Shell {
        fn new(name: String, location: String, term: String) -> Self {
            Shell {
                name,
                location,
                term
            }
        }

        #[cfg(target_os = "linux")]
        pub fn get() -> Self {
            let shell_path = match std::env::var("SHELL") {
                Ok(s) => s,
                Err(e) => {
                    eprintln!("Error (SHELL): {}", e);
                    String::from("/bin/sh")
                }
            };
            let shell = shell_path.split("/")
                            .collect::<Vec<&str>>()
                            .last()
                            .expect("error")
                            .to_owned();
            let term = match std::env::var("TERM") {
                Ok(s) => s,
                Err(e) => {
                    eprintln!("Error (TERM): {}", e);
                    String::from("xterm") // Sane default
                }
            };
            Shell::new(
                String::from(shell),
                String::from("/usr/bin/zsh"),
                term
            )
        }

        #[cfg(target_os = "macos")]
        pub fn get() -> Self {
            let shell_path = match std::env::var("SHELL") {
                Ok(s) => s,
                Err(e) => {
                    eprintln!("Error (SHELL): {}", e);
                    String::from("/bin/sh")
                }
            };
            let shell = shell_path.split("/")
                            .collect::<Vec<&str>>()
                            .last()
                            .expect("error")
                            .to_owned();
            let term = match std::env::var("TERM") {
                Ok(s) => s,
                Err(e) => {
                    eprintln!("Error (TERM): {}", e);
                    String::from("xterm") // Sane default
                }
            };
            Shell::new(
                String::from(shell),
                String::from(shell_path),
                String::from(term)
            )
        }

        #[cfg(target_os = "windows")]
        pub fn get() -> Self {
            let powershell = match std::env::var("PSModulePath") {
                Ok(s) => s,
                Err(e) => {
                    eprintln!("Error (PSModulePath): {}", e);
                    String::from("cmd")
                }
            };
            Shell::new(
                String::from(powershell),
                String::from(),
                String::from()
            )
        }
    }
}


#[test]
fn test_platform() {
    let plat = platform::Platform::get();
    println!("{}", plat);
}

#[test]
fn test_shell() {
    let shell = shell::Shell::get();
    println!("{:?}", shell);
}
